
///------------------------------------------------------------------------------/
///   @file main.cpp                                                          /
///------------------------------------------------------------------------------/
///   @par Full description
///   DisplayLink Task, application code
///
///   @par Bartosz Tarnowski 2022
///------------------------------------------------------------------------------/

#include "mainwindow.h"
#include "display.h"
#include <QApplication>
#include "ArgumentsReceiver.h"

#include <windows.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    ArgReceiver inputArgs;

#ifdef TESTS
    display disp(inputArgs);
    w.setDisplay(&disp);
    w.setWindowTitle(QString::fromStdString(disp.m_name));

    color tl,tr,bl,br;
    vector<tuple<displayMode,color,color,color,color,string>> vect;

    tl.red = 31; tl.green = 63; tl.blue = 31;
    tr.red = 0;  tr.green = 0;  tr.blue = 0;
    bl.red = 31; bl.green = 63; bl.blue = 31;
    br.red = 0;  br.green = 0;  br.blue = 0;

    vect.push_back(make_tuple(MODE1,tl,tr,bl,br,"1"));
    vect.push_back(make_tuple(MODE2,tl,tr,bl,br,"2"));
    vect.push_back(make_tuple(MODE3,tl,tr,bl,br,"3"));

    tl.red = 3;  tl.green = 4;  tl.blue = 22;
    tr.red = 3;  tr.green = 0;  tr.blue = 0;
    bl.red = 31; bl.green = 23; bl.blue = 1;
    br.red = 3;  br.green = 0;  br.blue = 5;

    vect.push_back(make_tuple(MODE1,tl,tr,bl,br,"4"));
    vect.push_back(make_tuple(MODE2,tl,tr,bl,br,"5"));
    vect.push_back(make_tuple(MODE3,tl,tr,bl,br,"6"));

    w.setTestVector(&vect);
#endif
    w.show();
    a.exec();
}
