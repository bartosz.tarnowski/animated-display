///------------------------------------------------------------------------------/
///   @file ArgReceiver.cpp                                                      /
///------------------------------------------------------------------------------/
///   @par Full description
///  ArgReceiver class definition for processing input arguments of main function.
///  It has specific purpose regarding display class.
///  It has converter function to transform input arguments from strings to color values
///
///   @par Bartosz Tarnowski 2022
///------------------------------------------------------------------------------/


#include "ArgumentsReceiver.h"
#include <QtCore>
#include <utility>

ArgReceiver::ArgReceiver(void):argTable{0,0,0,0}
{
        GetArguments();
}

void ArgReceiver::GetArguments(void)
{
    QStringList argList = QCoreApplication::arguments().at(1).split(' ');

    len    = argList.length();
    int i  = argList.length();
    if( i <= MAX_INPUT_ARG)
    {
        while(i--)
        {
            if(i==0) // here function ends
            {
                //string argument - display name
                StringArg = argList.at(i);
                return;
            }
            //argument conversion block for HEX arguments
            if(argList.at(i).contains('x') || argList.at(i).contains('X'))
            {
                bool result;
                argTable[i-1] =  argList.at(i).toUInt(&result,16);
                if(result == false)
                {
                    argTable[i-1] = 0;
                    validArrgs = false;
                }
            }
            //argument conversion block for DEC arguments
            else
            {
                bool result;
                argTable[i-1] =  argList.at(i).toUInt(&result,10);
                if(result == false)
                {
                    argTable[i-1] = 0;
                    validArrgs = false;
                }
            }
        }
    }
    else
    {
        validArrgs = false;
    }
}

bool ArgReceiver::isValid(void)
{
    return validArrgs;
}

ArgReceiver::operator tuple<color,color,color,color> ()
{
    tuple<color,color,color,color>  out(argTable[0],argTable[1],argTable[2],argTable[3]);
    return out;
}

ArgReceiver::operator tuple<color,color,color> ()
{
    tuple<color,color,color>        out(argTable[0],argTable[1],argTable[2]);
    return out;
}

ArgReceiver::operator tuple<color,color> ()
{
    tuple<color,color>              out(argTable[0],argTable[1]);
    return out;
}

ArgReceiver::operator color ()
{
    return argTable[0];
}
