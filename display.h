///------------------------------------------------------------------------------/
///   @file display.cpp                                                          /
///------------------------------------------------------------------------------/
///   @par Full description
///   Display and display calsses definition
///
///   @par Bartosz Tarnowski 2022
///------------------------------------------------------------------------------/


#ifndef DISPLAY_H_INCLUDED
#define DISPLAY_H_INCLUDED
#include <vector>
#include <string>
#include <cstdint>
#include "color.h"
#include "ArgumentsReceiver.h"

namespace {
 constexpr uint16_t Width = 16;
 constexpr uint16_t Height = 9;
}

struct Point
{
 int16_t x;
 int16_t y;
};

struct Dimension
{
 uint16_t width;
 uint16_t height;
};

// Base class of the task

class Display
{
    public:
      Display(const std::string& name);
     ~Display() = default;

     Dimension size() const;
     void draw(Point point, const uint16_t* pixels, uint16_t width);
     void present();

    public:
     std::string m_name;
     std::vector<color> m_frameBuffer;
};

typedef enum{
    MODE1,
    MODE2,
    MODE3,
} displayMode;

class display: public Display
{
public:

    //set of constructors
    display(void);
    display(const std::string& name, color Top_Left, color Top_Right);
    display(const std::string& name, color Top_Left, color Top_Right, color Bottom_Left);
    display(const std::string& name, color Top_Left, color Top_Right, color Bottom_Left, color Bottom_Rigth);

    display(ArgReceiver& args);


    bool setDisplay(const std::string &name, color Top_Left, color Top_Right);
    bool setDisplay(const std::string &name, color Top_Left, color Top_Right, color Bottom_Left);
    bool setDisplay(const std::string &name, color Top_Left, color Top_Right, color Bottom_Left, color Bottom_Rigth);
private:
    color TopLeft;
    color TopRight;
    color BottomLeft;
    color BottomRigth;

    void writeOneLine     (color Top_Left, colorGradient coef);
    void fillDisplayBuffer(color Top_Left, color Top_Right, uint8_t rows);
    bool processInputs    (color Top_Left, color Top_Right, color Bottom_Left);
    bool processInputs    (color Top_Left, color Top_Right, color Bottom_Left, color Bottom_Rigth);
};

#endif
