#include "color.h"
#include "math.h"

color::color(){}


color::operator QRgb()
{
    return ( ((int)red)<<19) | ( ((int)green)<<10) | ( ((int)blue)<<3);
}

color color::operator+(color& in)
{
    color out;
    out.red  = this->red   + in.red;
    out.blue = this->blue  + in.blue;
    out.green= this->green + in.green;
    return out;
}

void color::calculateFromGradient(color& base, colorGradient& grad, uint8_t iter)
{
    red   = base.red   + (uint8_t)round(grad.red   * iter);
    green = base.green + (uint8_t)round(grad.green * iter);
    blue  = base.blue  + (uint8_t)round(grad.blue  * iter);
}

color::color(int val)
{
    this->value = val;
}

colorDiff::colorDiff(color& a, color& b)
{
    diffRed   =   (int8_t)((int8_t)a.red   - (int8_t)b.red);
    diffGreen =   (int8_t)((int8_t)a.green - (int8_t)b.green);
    diffBlue  =   (int8_t)((int8_t)a.blue  - (int8_t)b.blue);
}

colorGradient::colorGradient(colorDiff& diff, int width)
{
    red   = (float)diff.diffRed   / (float)(width-1);
    green = (float)diff.diffGreen / (float)(width-1);
    blue  = (float)diff.diffBlue  / (float)(width-1);
}
