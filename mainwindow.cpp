///------------------------------------------------------------------------------/
///   @file mainwindow.cpp                                                          /
///------------------------------------------------------------------------------/
///   @par Full description
///   Mainwindow implemnts behaviour regarding displaying data from display class
///
///   @par Bartosz Tarnowski 2022
///------------------------------------------------------------------------------/
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtGui>
#include <QtCore>
#include <iostream>
#include <QObject>

#include <cstdio>
#include <cstdlib>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
#ifdef  TESTS
        QObject::connect(&timer,&QTimer::timeout,this,&MainWindow::timerSlot);
#endif
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


// Painting the MainWindow with data from display class
void MainWindow::paintEvent(QPaintEvent *e)
{
    if(m_currentDisplay == nullptr);
    else
    {
        QPainter painter(this);
        painter.eraseRect(0,0,1600,900);

        for (int i = 0; i<Height ; i++){
        for (int j = 0; j< Width ; j++){
             int idx = j+i*Width;

            QRect rect(j*100+5,i*100+5,100,100);

            color* temp = m_currentDisplay->m_frameBuffer.data();   // Getting data from display class
            QRgb   coll = QRgb(temp[j+i*Width]);                   // converting function in union

            painter.fillRect(rect,coll);
            painter.drawRect(rect);

            QString rgb565,red,green,blue;

            rgb565.append(QString("%1 %2").arg(temp[idx].value,0,16).arg(coll,0,16));
            red   .append(QString("R: %1").arg(temp[idx].red));
            green .append(QString("G: %1").arg(temp[idx].green));
            blue  .append(QString("B: %1").arg(temp[idx].blue));

            painter.drawText(j*100+8,i*100+15,rgb565);
            painter.drawText(j*100+8,i*100+35,red   );
            painter.drawText(j*100+8,i*100+55,green );
            painter.drawText(j*100+8,i*100+75,blue  );
        }}
    }
#ifdef  TESTS
        timer.start(40);
#endif
}

void MainWindow::timerSlot(void)
{
   if(testVector != nullptr)
   {
       // Display Animation
        if(0 == testVector->size())
        {
            static display disp5;
            static int a=0,b=15678,c=0x700F,d=32300;  // random initial values for animated display
            disp5.setDisplay("ANIMATED DISPLAY",a++,b++,c++,d++); // INCREMENTATION
            setDisplay(&disp5);

            this->setWindowTitle(QString::fromStdString(m_currentDisplay->m_name));
            repaint();

            return;
        }

        tuple<displayMode,color,color,color,color,string> item = testVector->back();
                                                          testVector->pop_back();
        string j = get<5>(item);
        switch(get<0>(item))
        {
            case MODE1:
            {
                static display disp1;
                disp1.setDisplay(get<5>(item),get<1>(item),get<2>(item));
                setDisplay(&disp1);
                break;
            }

            case MODE2:
            {
                static display disp2;
                disp2.setDisplay(get<5>(item),get<1>(item),get<2>(item),get<3>(item));
                setDisplay(&disp2);
                break;
            }

            case MODE3:
            {
                static display disp3;
                disp3.setDisplay(get<5>(item),get<1>(item),get<2>(item),get<3>(item),get<4>(item));
                setDisplay(&disp3);
                break;
            }
        }
        this->setWindowTitle(QString::fromStdString(m_currentDisplay->m_name));
        repaint();
   }
}

bool MainWindow::setDisplay(Display* item)
{
    if(item != nullptr)
    {
        m_currentDisplay = item;
        return true;
    }
    else return false;
}

bool MainWindow::setTestVector(vector<tuple<displayMode,color,color,color,color,string>>* vect)
{
        testVector = vect;
        return true;
}
