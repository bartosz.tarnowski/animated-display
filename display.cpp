
///------------------------------------------------------------------------------/
///   @file display.cpp                                                          /
///------------------------------------------------------------------------------/
///   @par Full description
///   Display and display calsses implementation
///
///   @par Bartosz Tarnowski 2022
///------------------------------------------------------------------------------/

#include "display.h"
#include "math.h"
#include <cstring>
#include "ArgumentsReceiver.h"

Display::Display(const std::string& name): m_name(name), m_frameBuffer(Width * Height)
{
}

Dimension Display::size() const
{
    return { Width, Height };
}

void Display::draw(const Point point,   const uint16_t *pixels, const uint16_t width)
{
    std::memcpy(&m_frameBuffer[point.y * Width + point.x],  pixels,   width * sizeof(uint16_t));
}

void Display::present()
{
     auto pixels = m_frameBuffer.data();

     for (int y = 0; y < Height; ++y){
     for (int x = 0; x < Width; ++x) {   printf("%04X ", *pixels++);}
                                         printf("\n");              }
}

display::display():Display(" "){}


display::display(const std::string& name, color Top_Left, color Top_Right): Display(name)
{

}

display::display(const std::string& name, color Top_Left, color Top_Right, color Bottom_Left): Display(name)
{

}

display::display(const std::string& name, color Top_Left, color Top_Right, color Bottom_Left, color Bottom_Rigth): Display(name)
{

}

display::display(ArgReceiver& args):Display(args.StringArg.toStdString()),TopLeft(args.argTable[0]),TopRight(args.argTable[1]),BottomLeft(args.argTable[2]),BottomRigth(args.argTable[3])
{
    if(args.isValid()){
        switch(args.len)
        {
            case 3:
            {
                m_frameBuffer.clear();
                fillDisplayBuffer(TopLeft,TopRight,Height);
                break;
            }
            case 4:
            {
                processInputs(TopLeft,TopRight,BottomLeft);
                break;
            }
            case 5:
            {
                processInputs(TopLeft,TopRight,BottomLeft,BottomRigth);
                break;
            }

              default:
            {

            }
        }
    }
}

bool display::setDisplay(const std::string& name, color Top_Left, color Top_Right)
{
         m_name = name;     // const issue - mutable
        TopLeft = Top_Left;
       TopRight = Top_Right;

    m_frameBuffer.clear();

    fillDisplayBuffer(Top_Left,Top_Right,Height);
    return true;
}

bool display::setDisplay(const std::string& name, color Top_Left, color Top_Right, color Bottom_Left)
{
         m_name = name;     // const issue - mutable
        TopLeft = Top_Left;
       TopRight = Top_Right;
     BottomLeft = Bottom_Left;

    processInputs(Top_Left, Top_Right, Bottom_Left);
    return true;
}

bool display::setDisplay(const std::string& name, color Top_Left, color Top_Right, color Bottom_Left, color Bottom_Rigth)
{
         m_name = name;     // const issue - mutable
        TopLeft = Top_Left;
       TopRight = Top_Right;
     BottomLeft = Bottom_Left;
    BottomRigth = Bottom_Rigth;

    processInputs(Top_Left, Top_Right, Bottom_Left, Bottom_Rigth);
    return true;
}


/*----------------------------------------------------------------------------/
 * Description: processInputs creates the whole content of m_frameBuffer vector
 * this one overloaded function gets 3 initlial values for border locations of display.
 * It calculates data for every row of display eg calculates arguments for fillDisplayBuffer function
  --------------------------------------------------------------------------/*/

bool display::processInputs(color Top_Left, color Top_Right, color Bottom_Left)
{
    m_frameBuffer.clear();

    for(int i=0;i<Height;i++)
    {
        colorDiff diff(Bottom_Left,Top_Left);
        colorGradient coef(diff,Height);

        color leftcolor;
        leftcolor.value=0;

        fillDisplayBuffer(leftcolor,Top_Right,1);
    }
    return true; // refactor
}

/*----------------------------------------------------------------------------/
 * Description: processInputs creates the whole content of m_frameBuffer vector
 * this one overloaded function gets 4 initlial values for 4 border locations of display.
 * It calculates data for every row of display eg calculates arguments for fillDisplayBuffer function
  --------------------------------------------------------------------------/*/

bool display::processInputs(color Top_Left, color Top_Right, color Bottom_Left, color Bottom_Rigth)
{
    m_frameBuffer.clear();

    for(int i=0;i<Height;i++)
    {
        colorDiff        diffLeft(Bottom_Left,Top_Left);
        colorGradient    coefL(diffLeft,Height);
        color            leftcolor;
                         leftcolor.calculateFromGradient(Top_Left, coefL, i);

        colorDiff        diffRight(Bottom_Rigth,Top_Right);
        colorGradient    coefR(diffRight,Height);
        color            rightcolor;
                         rightcolor.calculateFromGradient(Top_Right, coefR, i);

        fillDisplayBuffer(leftcolor,rightcolor,1);
    }
    return true; // refactor
}

/*----------------------------------------------------------------------------/
 * Description: writeOneLine It adds set of values to the m frameBuffer vector
 * it depends on the first (most left) value of display color in the row and
 *            on the calculated coefficient eg on the difference between adjacent values per pixel
 *
 * Input args:
 *              MostLeftValue - the first RGB value goint to be placed in the m_frameBuffer for current row
 *              coef: set of values for the R, G, B components
  --------------------------------------------------------------------------/*/
void display::fillDisplayBuffer(color Top_Left, color Top_Right, uint8_t rows)
{
    colorDiff diff(Top_Right,Top_Left);
    colorGradient coef(diff,Width);

   for(int j=0;j<rows;j++){
                                writeOneLine(Top_Left,coef);
   }
}


/*----------------------------------------------------------------------------/
 * Description: writeOneLine adds set of values to the m_frameBuffer vector,
 * it depends on the first (most left) value of display color in the row and
 *            on the calculated coefficient eg on the difference between adjacent values per pixel
 *
 * Input args:
 *              MostLeftValue - the first RGB value goint to be placed in the m_frameBuffer for current row
 *              coef: set of values for the R, G, B components
  --------------------------------------------------------------------------/*/
void display::writeOneLine(color MostLeftValue, colorGradient coef)
{
    for(int i=0;i<Width;i++)
    {
        color col;
        col.value=0;
        float temp;
        temp = round(coef.red*i);
        col.red = MostLeftValue.red + (uint8_t)temp;

        temp = round(coef.green*i);
        col.green = MostLeftValue.green + (uint8_t)temp;

        temp = round(coef.blue*i);
        col.blue = MostLeftValue.blue + (uint8_t)temp;

        m_frameBuffer.push_back(col);
    }
}
