///------------------------------------------------------------------------------/
///   @file mainwindow.h                                                          /
///------------------------------------------------------------------------------/
///   @par Full description
///   Mainwindow class definition
///
///   @par Bartosz Tarnowski 2022
///------------------------------------------------------------------------------/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui>
#include <QtCore>
#include <QMainWindow>

#include <stdio.h>
#include <stdlib.h>

#include <display.h>
#include <color.h>

//#define TESTS

using namespace std;
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    bool setDisplay(Display* item);
    bool setTestVector(vector<tuple<displayMode,color,color,color,color,string>>* vect);

private:
    Ui::MainWindow *ui;
    QTimer timer;

protected:
    Display* m_currentDisplay = nullptr;
    vector<tuple<displayMode,color,color,color,color,string>>* testVector=nullptr;

    // for painting on mainwindow GUI object
    void paintEvent(QPaintEvent *e);

public
    Q_SLOTS:
void timerSlot(void);

    Q_SIGNALS:
};
#endif // MAINWINDOW_H
