#ifndef COLOR_H
#define COLOR_H
#include <qrgb.h>

struct colorGradient;

#pragma pack(push,2)
union color
{
  struct{
  uint16_t blue:5;
  uint16_t green:6;
  uint16_t red:5;
  };
  uint16_t value;

  color();
  color(int in);
  operator QRgb();
  color operator+ (color&);
  void calculateFromGradient(color &base, colorGradient &grad, uint8_t iter);
};
#pragma pack(pop)


struct colorDiff
{
    colorDiff(color& a, color& b);
    int8_t diffRed;
    int8_t diffGreen;
    int8_t diffBlue;
};

struct colorGradient
{
    colorGradient(colorDiff& diff, int width);
    float red;
    float green;
    float blue;
};
#endif // COLOR_H
