///------------------------------------------------------------------------------/
///   @file ArgReceiver.cpp                                                      /
///------------------------------------------------------------------------------/
///   @par Full description
///  ArgReceiver class definition for processing input arguments of main function.
///  It has specific purpose regarding display class.
///  It has converter function to transform input arguments from strings to color values
///
///   @par Bartosz Tarnowski 2022
///------------------------------------------------------------------------------/

#ifndef ArgReceiverH
#define ArgReceiverH
#include <utility>
#include <QString>
#include <utility>
#include "color.h"

#define MAX_INPUT_ARG 5

using namespace std;

class ArgReceiver{
public:

    explicit ArgReceiver(void);
    uint16_t argTable[MAX_INPUT_ARG];
    QString StringArg;
    uint16_t len;

    // set of converting functions

    operator tuple<color,color,color,color> ();
    operator tuple<color,color,color> ();
    operator tuple<color,color> ();
    operator color();

    bool isValid(void);

private:
   ArgReceiver(ArgReceiver&);               // inhibit copy constructor
   ArgReceiver  operator=(ArgReceiver&);    // inhibit assignment operator
   bool validArrgs;
   void GetArguments(void);                 // the core function to process arguments, used in the default constructor

};

#endif // ArgReceiver_H
